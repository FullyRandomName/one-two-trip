package mistamek.com.onetwoassignment.features.serp.presentation.serp.model

data class TourVO(
    val id: Long,
    val name: String,
    val priceDescription: String,
    val variantsDescription: String
)