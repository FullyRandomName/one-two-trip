package mistamek.com.onetwoassignment.features.serp.domain.model

data class Flight(
    val id: Long,
    val company: Company,
    val departure: String,
    val arrival: String,
    val price: Long
)