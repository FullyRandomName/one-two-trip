package mistamek.com.onetwoassignment.features.serp.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "company")
data class CompanyEntity(
    @PrimaryKey var companyId: Long,
    var name: String
)