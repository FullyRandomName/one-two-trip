package mistamek.com.onetwoassignment.features.serp.presentation.detail.model

import android.content.res.Resources
import mistamek.com.onetwoassignment.R
import mistamek.com.onetwoassignment.features.serp.domain.model.Flight
import mistamek.com.onetwoassignment.features.serp.domain.model.Tour
import javax.inject.Inject

interface DetailVOMapper {
    fun mapToDetailVO(tour: Tour, flight: Flight): DetailVO
}

class DetailVOMapperImpl @Inject constructor(
    private val resources: Resources
) : DetailVOMapper {

    override fun mapToDetailVO(tour: Tour, flight: Flight): DetailVO =
        flight.run {
            DetailVO(
                tourId = tour.id,
                flightId = id,
                name = company.name,
                price = resources.getString(R.string.serp_price_description, price + tour.price)
            )
        }
}