package mistamek.com.onetwoassignment.features.serp.presentation.filter.di

import dagger.Module
import dagger.Provides
import mistamek.com.onetwoassignment.features.serp.domain.SerpInteractor
import mistamek.com.onetwoassignment.features.serp.presentation.filter.FilterDialog
import mistamek.com.onetwoassignment.features.serp.presentation.filter.FilterPresenter
import mistamek.com.onetwoassignment.features.serp.presentation.filter.FilterPresenterImpl
import mistamek.com.onetwoassignment.features.serp.presentation.filter.model.FilterVOMapper
import mistamek.com.onetwoassignment.features.serp.presentation.filter.model.FilterVOMapperImpl

@Module
class FilterDialogModule(private val filterDialog: FilterDialog) {

    @Provides
    fun provideFilterPresenter(
        serpInteractor: SerpInteractor,
        filterVOMapper: FilterVOMapper
    ): FilterPresenter =
        FilterPresenterImpl(
            filterDialog,
            serpInteractor,
            filterVOMapper
        )

    @Provides
    fun provideFilterVOMapper(): FilterVOMapper = FilterVOMapperImpl()
}