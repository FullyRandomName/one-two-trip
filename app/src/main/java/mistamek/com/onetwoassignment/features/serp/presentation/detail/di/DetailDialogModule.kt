package mistamek.com.onetwoassignment.features.serp.presentation.detail.di

import dagger.Module
import dagger.Provides
import mistamek.com.onetwoassignment.features.serp.domain.SerpInteractor
import mistamek.com.onetwoassignment.features.serp.presentation.detail.DetailDialog
import mistamek.com.onetwoassignment.features.serp.presentation.detail.DetailPresenter
import mistamek.com.onetwoassignment.features.serp.presentation.detail.DetailPresenterImpl
import mistamek.com.onetwoassignment.features.serp.presentation.detail.model.DetailVOMapper
import mistamek.com.onetwoassignment.features.serp.presentation.detail.model.DetailVOMapperImpl

@Module
class DetailDialogModule(private val detailDialog: DetailDialog) {

    @Provides
    fun provideDetailPresenter(
        serpInteractor: SerpInteractor,
        detailVOMapper: DetailVOMapper
    ): DetailPresenter =
        DetailPresenterImpl(
            tourId = detailDialog.getSelectedTourId(),
            detailView = detailDialog,
            serpInteractor = serpInteractor,
            detailVOMapper = detailVOMapper
        )

    @Provides
    fun providesDetailVOMapper(): DetailVOMapper = DetailVOMapperImpl(detailDialog.resources)
}