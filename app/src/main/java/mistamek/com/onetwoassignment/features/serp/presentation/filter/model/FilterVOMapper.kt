package mistamek.com.onetwoassignment.features.serp.presentation.filter.model

import mistamek.com.onetwoassignment.features.serp.domain.model.Company
import javax.inject.Inject

interface FilterVOMapper {
    fun mapCompanyToCompanyVO(company: Company): FilterVO
}

class FilterVOMapperImpl @Inject constructor() : FilterVOMapper {

    override fun mapCompanyToCompanyVO(company: Company): FilterVO =
        company.run {
            FilterVO(
                id = id,
                name = name
            )
        }
}