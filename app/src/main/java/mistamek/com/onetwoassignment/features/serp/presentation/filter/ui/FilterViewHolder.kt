package mistamek.com.onetwoassignment.features.serp.presentation.filter.ui

import android.view.ViewGroup
import kotlinx.android.synthetic.main.serp_filter_view_holder.*
import mistamek.com.onetwoassignment.R
import mistamek.com.onetwoassignment.core.platform.BaseViewHolder
import mistamek.com.onetwoassignment.features.serp.presentation.filter.model.FilterVO

class FilterViewHolder(parent: ViewGroup, clickListener: (position: Int) -> Unit) :
    BaseViewHolder(R.layout.serp_filter_view_holder, parent) {

    init {
        itemView.setOnClickListener {
            clickListener(adapterPosition)
        }
    }

    fun bind(item: FilterVO) {
        filterNameTextView.text = item.name
    }
}