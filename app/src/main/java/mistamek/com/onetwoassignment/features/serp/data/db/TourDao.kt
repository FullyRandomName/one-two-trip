package mistamek.com.onetwoassignment.features.serp.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import mistamek.com.onetwoassignment.features.serp.data.db.model.TourEntity

@Dao
interface TourDao {

    @Query("SELECT * from tour ORDER BY positionId ASC")
    fun getTours(): List<TourEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTours(tours: List<TourEntity>)

    @Query("DELETE from tour")
    fun deleteTours()
}