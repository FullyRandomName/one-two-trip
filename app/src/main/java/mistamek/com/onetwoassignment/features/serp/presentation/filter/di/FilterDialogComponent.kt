package mistamek.com.onetwoassignment.features.serp.presentation.filter.di

import dagger.Component
import mistamek.com.onetwoassignment.di.ApplicationComponent
import mistamek.com.onetwoassignment.di.FragmentScope
import mistamek.com.onetwoassignment.features.serp.presentation.filter.FilterDialog

@FragmentScope
@Component(dependencies = [ApplicationComponent::class], modules = [FilterDialogModule::class])
interface FilterDialogComponent {

    fun inject(fragment: FilterDialog)
}