package mistamek.com.onetwoassignment.features.serp.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tour")
data class TourEntity(
    @PrimaryKey var tourId: Long,
    var positionId: Int,
    var flightsIds: List<Long>,
    var name: String,
    var price: Long
)