package mistamek.com.onetwoassignment.features.serp.data.network.model

data class HotelsResponse(
    val hotels: List<HotelItem>
) {

    data class HotelItem(
        val id: Long,
        val flights: List<Long>,
        val name: String,
        val price: Long
    )
}