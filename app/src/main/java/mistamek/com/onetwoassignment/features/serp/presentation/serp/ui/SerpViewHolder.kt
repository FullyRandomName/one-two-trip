package mistamek.com.onetwoassignment.features.serp.presentation.serp.ui

import android.view.ViewGroup
import kotlinx.android.synthetic.main.serp_view_holder.*
import mistamek.com.onetwoassignment.R
import mistamek.com.onetwoassignment.core.platform.BaseViewHolder
import mistamek.com.onetwoassignment.features.serp.presentation.serp.model.TourVO

class SerpViewHolder(parent: ViewGroup, clickListener: (position: Int) -> Unit) :
    BaseViewHolder(R.layout.serp_view_holder, parent) {

    init {
        itemView.setOnClickListener {
            clickListener(adapterPosition)
        }
    }

    fun bind(tour: TourVO) {
        with(tour) {
            tourNameTextView.text = name
            tourPriceTextView.text = priceDescription
            tourVariantsTextView.text = variantsDescription
        }
    }
}