package mistamek.com.onetwoassignment.features.serp.presentation.serp.di

import dagger.Module
import dagger.Provides
import mistamek.com.onetwoassignment.features.serp.SerpRouter
import mistamek.com.onetwoassignment.features.serp.SerpRouterImpl
import mistamek.com.onetwoassignment.features.serp.domain.SerpInteractor
import mistamek.com.onetwoassignment.features.serp.presentation.serp.*
import mistamek.com.onetwoassignment.features.serp.presentation.serp.model.SerpVOMapper
import mistamek.com.onetwoassignment.features.serp.presentation.serp.model.SerpVOMapperImpl

// Google перешел на android-x пакет, но dagger-android все еще использует appcompat библиотеку
// для нормального провайда view контракта пришлось использовать два модуля
// при написании более крупного приложения с нуля, скорее всего сделал бы форк dagger-android с поддержкой android-x
@Module
class SerpFragmentModule(private val serpFragment: SerpFragment) {

    @Provides
    fun provideSerpPresenter(
        serpInteractor: SerpInteractor,
        serpVOMapper: SerpVOMapper
    ): SerpPresenter =
        SerpPresenterImpl(
            serpView = serpFragment,
            serpInteractor = serpInteractor,
            serpVOMapper = serpVOMapper,
            selectedCompanyId = null
        )

    @Provides
    fun providesSerpRouter(): SerpRouter = SerpRouterImpl(serpFragment)

    @Provides
    fun provideSerpStateHandler(): SerpStateHandler = SerpStateHandlerImpl()

    @Provides
    fun providesSerpVOMapper(): SerpVOMapper = SerpVOMapperImpl(serpFragment.resources)
}


