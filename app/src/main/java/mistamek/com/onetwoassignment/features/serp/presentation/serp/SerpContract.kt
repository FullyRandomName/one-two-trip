package mistamek.com.onetwoassignment.features.serp.presentation.serp

import mistamek.com.onetwoassignment.core.platform.BasePresenter
import mistamek.com.onetwoassignment.core.platform.BaseView
import mistamek.com.onetwoassignment.features.serp.presentation.serp.model.TourVO

interface SerpView : BaseView {

    fun showLoading()

    fun hideLoading()

    fun showError(text: String)

    fun showTours(items: List<TourVO>)
}

interface SerpPresenter : BasePresenter {
    var selectedCompanyId: Long?

    fun onFilterApply()
}