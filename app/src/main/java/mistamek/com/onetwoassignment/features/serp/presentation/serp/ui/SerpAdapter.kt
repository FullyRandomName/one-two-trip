package mistamek.com.onetwoassignment.features.serp.presentation.serp.ui

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import mistamek.com.onetwoassignment.features.serp.presentation.serp.model.TourVO

class SerpAdapter(private val clickListener: (item: Long) -> Unit) :
    ListAdapter<TourVO, SerpViewHolder>(diffCallback) {

    companion object {
        val diffCallback = object : DiffUtil.ItemCallback<TourVO>() {
            override fun areItemsTheSame(oldItem: TourVO, newItem: TourVO): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: TourVO, newItem: TourVO): Boolean {
                return oldItem == newItem
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SerpViewHolder =
        SerpViewHolder(parent) { clickListener(getItem(it).id) }

    override fun onBindViewHolder(holder: SerpViewHolder, position: Int) = holder.bind(getItem(position))
}