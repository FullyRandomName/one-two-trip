package mistamek.com.onetwoassignment.features.serp.presentation.detail.ui

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import mistamek.com.onetwoassignment.features.serp.presentation.detail.model.DetailVO

class DetailAdapter(private val clickListener: (tourId: Long, flightId: Long) -> Unit) :
    RecyclerView.Adapter<DetailViewHolder>() {

    var items: List<DetailVO> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailViewHolder =
        DetailViewHolder(parent) {
            items[it].also { item ->
                clickListener(item.tourId, item.flightId)
            }
        }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: DetailViewHolder, position: Int) {
        holder.bind(items[position])
    }
}