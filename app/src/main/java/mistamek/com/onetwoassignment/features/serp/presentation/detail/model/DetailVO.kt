package mistamek.com.onetwoassignment.features.serp.presentation.detail.model

data class DetailVO(
    val tourId: Long,
    val flightId: Long,
    val name: String,
    val price: String
)