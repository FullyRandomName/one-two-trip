package mistamek.com.onetwoassignment.features.serp.presentation.filter

import mistamek.com.onetwoassignment.core.platform.BasePresenter
import mistamek.com.onetwoassignment.core.platform.BaseView
import mistamek.com.onetwoassignment.features.serp.presentation.filter.model.FilterVO

interface FilterView : BaseView {

    fun showLoading()

    fun hideLoading()

    fun showError(text: String)

    fun showCompanies(items: List<FilterVO>)
}

interface FilterPresenter : BasePresenter {
}