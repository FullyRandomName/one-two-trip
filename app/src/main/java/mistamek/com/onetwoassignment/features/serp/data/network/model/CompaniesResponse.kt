package mistamek.com.onetwoassignment.features.serp.data.network.model

data class CompaniesResponse(
    val companies: List<CompanyItem>
) {
    data class CompanyItem(
        val id: Long,
        val name: String
    )
}