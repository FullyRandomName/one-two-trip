package mistamek.com.onetwoassignment.features.serp.presentation.serp

import android.os.Bundle
import android.os.Parcelable
import javax.inject.Inject

interface SerpStateHandler {
    var recyclerViewState: Parcelable?
    var previousDataHash: Int?
    var selectedCompanyId: Long?

    fun onSaveState(instanceState: Bundle?)

    fun onRestoreState(instanceState: Bundle?)
}

class SerpStateHandlerImpl @Inject constructor() : SerpStateHandler {

    companion object {
        private const val RECYCLER_VIEW_STATE = "SerpStateHandler.RecyclerState"
        private const val PREVIOUS_DATA_HASH = "SerpStateHandler.PreviousDataHash"
        private const val SELECTED_COMPANY_ID = "SerpStateHandler.SelectedCompanyId"
    }

    override var recyclerViewState: Parcelable? = null
    override var previousDataHash: Int? = null
    override var selectedCompanyId: Long? = null

    override fun onSaveState(instanceState: Bundle?) {
        instanceState?.apply {
            recyclerViewState?.also { putParcelable(RECYCLER_VIEW_STATE, it) }
            previousDataHash?.also { putInt(PREVIOUS_DATA_HASH, it) }
            selectedCompanyId?.also { putLong(SELECTED_COMPANY_ID, it) }
        }
    }

    override fun onRestoreState(instanceState: Bundle?) {
        instanceState?.apply {
            if (containsKey(RECYCLER_VIEW_STATE)) {
                recyclerViewState = getParcelable(RECYCLER_VIEW_STATE)
            }
            if (containsKey(PREVIOUS_DATA_HASH)) {
                previousDataHash = getInt(PREVIOUS_DATA_HASH)
            }
            if (containsKey(SELECTED_COMPANY_ID)) {
                selectedCompanyId = getLong(SELECTED_COMPANY_ID)
            }
        }
    }
}