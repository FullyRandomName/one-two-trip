package mistamek.com.onetwoassignment.features.serp.presentation.serp

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.default_fragment.*
import mistamek.com.onetwoassignment.R
import mistamek.com.onetwoassignment.core.App
import mistamek.com.onetwoassignment.features.serp.SerpRouter
import mistamek.com.onetwoassignment.features.serp.presentation.filter.FilterDialog
import mistamek.com.onetwoassignment.features.serp.presentation.serp.di.DaggerSerpFragmentComponent
import mistamek.com.onetwoassignment.features.serp.presentation.serp.di.SerpFragmentModule
import mistamek.com.onetwoassignment.features.serp.presentation.serp.model.TourVO
import mistamek.com.onetwoassignment.features.serp.presentation.serp.ui.SerpAdapter
import mistamek.com.onetwoassignment.util.gone
import mistamek.com.onetwoassignment.util.show
import mistamek.com.onetwoassignment.util.snackbar
import javax.inject.Inject

class SerpFragment : Fragment(), SerpView {

    @Inject
    lateinit var presenter: SerpPresenter
    @Inject
    lateinit var serpStateHandler: SerpStateHandler
    @Inject
    lateinit var serpRouter: SerpRouter

    private val serpAdapter = SerpAdapter {
        serpRouter.showDetail(it)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerSerpFragmentComponent.builder()
            .applicationComponent((requireContext().applicationContext as App).applicationComponent)
            .serpFragmentModule(SerpFragmentModule(this))
            .build()
            .inject(this)
        setHasOptionsMenu(true)
        serpStateHandler.onRestoreState(savedInstanceState)
        presenter.selectedCompanyId = serpStateHandler.selectedCompanyId
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.default_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecycler()
        presenter.onViewReady(savedInstanceState != null)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.serp_menu, menu)
        menu?.findItem(R.id.filter_menu)?.isVisible = serpAdapter.itemCount > 0
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.filter_menu -> {
                serpRouter.showFilter()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onStop() {
        super.onStop()
        presenter.onViewDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        contentRecyclerView.layoutManager?.onSaveInstanceState()?.also { savedState ->
            serpStateHandler.recyclerViewState = savedState
        }
        serpStateHandler.onSaveState(outState)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when {
            requestCode == SerpRouter.FILTER_TARGET_CODE && resultCode == Activity.RESULT_OK -> {
                val companyId = data?.takeIf { it.hasExtra(FilterDialog.SELECTED_COMPANY) }
                    ?.getLongExtra(FilterDialog.SELECTED_COMPANY, 0)
                serpStateHandler.selectedCompanyId = companyId
                presenter.selectedCompanyId = companyId
                presenter.onFilterApply()
            }
        }
    }

    override fun showLoading() {
        loadingProgress.show()
    }

    override fun hideLoading() {
        loadingProgress.gone()
    }

    override fun showError(text: String) {
        rootFrame.snackbar(text)
    }

    override fun showTours(items: List<TourVO>) {
        val adapterWasEmpty = serpAdapter.itemCount == 0
        serpAdapter.submitList(items)
        val newDataHash = items.hashCode()

        // restoring scroll state after recreation or scrolling to top with new items
        when {
            adapterWasEmpty && newDataHash == serpStateHandler.previousDataHash -> {
                contentRecyclerView.layoutManager?.onRestoreInstanceState(serpStateHandler.recyclerViewState)
            }
            !adapterWasEmpty && newDataHash != serpStateHandler.previousDataHash -> {
                contentRecyclerView.scrollToPosition(0)
            }
        }
        serpStateHandler.previousDataHash = newDataHash
        activity?.invalidateOptionsMenu()
    }

    private fun initRecycler() {
        with(contentRecyclerView) {
            adapter = serpAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }
}