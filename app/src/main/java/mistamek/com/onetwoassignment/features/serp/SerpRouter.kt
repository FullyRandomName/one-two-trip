package mistamek.com.onetwoassignment.features.serp

import androidx.fragment.app.Fragment
import mistamek.com.onetwoassignment.features.serp.presentation.detail.DetailDialog
import mistamek.com.onetwoassignment.features.serp.presentation.filter.FilterDialog
import javax.inject.Inject

interface SerpRouter {

    companion object {
        const val FILTER_TARGET_CODE = 253455
        const val DETAIL_DIALOG_CODE = 253456
    }

    fun showFilter()

    fun showDetail(tourId: Long)
}

class SerpRouterImpl @Inject constructor(
    private val rootFragment: Fragment
) : SerpRouter {

    companion object {
        const val FILTER_DIALOG_TAG = "SerpFragment.FilterDialog"
        const val DETAIL_DIALOG_TAG = "SerpFragment.DetailDialog"
    }

    override fun showFilter() {
        FilterDialog().also { filterDialog ->
            filterDialog.setTargetFragment(rootFragment, SerpRouter.FILTER_TARGET_CODE)
            filterDialog.show(rootFragment.fragmentManager, FILTER_DIALOG_TAG)
        }
    }


    override fun showDetail(tourId: Long) {
        DetailDialog.newInstance(tourId).also { detailDialog ->
            detailDialog.setTargetFragment(rootFragment, SerpRouter.DETAIL_DIALOG_CODE)
            detailDialog.show(rootFragment.fragmentManager, DETAIL_DIALOG_TAG)
        }
    }
}