package mistamek.com.onetwoassignment.features.serp.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import mistamek.com.onetwoassignment.features.serp.data.db.model.FlightEntity

@Dao
interface FlightDao {

    @Query("SELECT * from flight")
    fun getFlights(): List<FlightEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFlights(flight: List<FlightEntity>)

    @Query("DELETE from flight")
    fun deleteFlights()
}