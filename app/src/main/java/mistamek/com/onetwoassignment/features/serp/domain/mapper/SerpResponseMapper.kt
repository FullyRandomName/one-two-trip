package mistamek.com.onetwoassignment.features.serp.domain.mapper

import androidx.collection.LongSparseArray
import mistamek.com.onetwoassignment.features.serp.data.network.model.CompaniesResponse
import mistamek.com.onetwoassignment.features.serp.data.network.model.FlightsResponse
import mistamek.com.onetwoassignment.features.serp.data.network.model.HotelsResponse
import mistamek.com.onetwoassignment.features.serp.domain.model.Company
import mistamek.com.onetwoassignment.features.serp.domain.model.Flight
import mistamek.com.onetwoassignment.features.serp.domain.model.Tour

object SerpResponseMapper {

    fun mapFlightResponseToFlight(
        flightItem: FlightsResponse.FlightItem,
        companiesArray: LongSparseArray<Company>
    ): Flight =
        flightItem.run {
            Flight(
                id = id,
                company = companiesArray[companyId]!!,
                departure = departure,
                arrival = arrival,
                price = price
            )
        }

    fun mapCompanyResponseToCompany(companyItem: CompaniesResponse.CompanyItem): Company =
        companyItem.run {
            Company(
                id = id,
                name = name
            )
        }

    fun mapHotelResponseToTour(
        hotelItem: HotelsResponse.HotelItem,
        flightsArray: LongSparseArray<Flight>
    ): Tour =
        hotelItem.run {
            Tour(
                id = id,
                flights = flights.mapNotNull { flightsArray[it] },
                name = name,
                price = price
            )
        }
}