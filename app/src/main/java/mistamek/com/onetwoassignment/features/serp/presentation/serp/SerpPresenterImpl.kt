package mistamek.com.onetwoassignment.features.serp.presentation.serp

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import mistamek.com.onetwoassignment.R
import mistamek.com.onetwoassignment.features.serp.domain.SerpInteractor
import mistamek.com.onetwoassignment.features.serp.presentation.serp.model.SerpVOMapper
import mistamek.com.onetwoassignment.util.plusAssign
import javax.inject.Inject

class SerpPresenterImpl @Inject constructor(
    private val serpView: SerpView,
    private val serpInteractor: SerpInteractor,
    private val serpVOMapper: SerpVOMapper,
    override var selectedCompanyId: Long?
) : SerpPresenter {

    private val compositeDisposable = CompositeDisposable()

    override fun onViewReady(wasRecreated: Boolean) {
        reloadSerp(wasRecreated)
    }

    override fun onFilterApply() {
        reloadSerp(true)
    }

    private fun reloadSerp(canUserCache: Boolean) {
        compositeDisposable += serpInteractor.getTours(canUserCache)
            .observeOn(Schedulers.computation())
            .map { tours ->
                tours.asSequence()
                    .filter { tour ->
                        // if tour has at least one flight with companyId
                        selectedCompanyId?.let { companyId ->
                            tour.flights.fold(false) { acc, next ->
                                acc || next.company.id == companyId
                            }
                        } ?: true
                    }
                    .map { tour -> serpVOMapper.mapTourToTourVO(tour) }
                    .toList()
            }
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { serpView.showLoading() }
            .doFinally { serpView.hideLoading() }
            .subscribe({
                serpView.showTours(it)
            }, {
                serpView.showError(serpView.requireContext().getString(R.string.default_error))
            })
    }

    override fun onViewDestroy() {
        compositeDisposable.dispose()
    }
}