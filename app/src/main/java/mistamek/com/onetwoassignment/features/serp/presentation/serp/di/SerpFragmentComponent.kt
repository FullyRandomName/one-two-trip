package mistamek.com.onetwoassignment.features.serp.presentation.serp.di

import dagger.Component
import mistamek.com.onetwoassignment.di.ApplicationComponent
import mistamek.com.onetwoassignment.di.FragmentScope
import mistamek.com.onetwoassignment.features.serp.presentation.serp.SerpFragment

@FragmentScope
@Component(dependencies = [ApplicationComponent::class], modules = [SerpFragmentModule::class])
interface SerpFragmentComponent {

    fun inject(fragment: SerpFragment)
}