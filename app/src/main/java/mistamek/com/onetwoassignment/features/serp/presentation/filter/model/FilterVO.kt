package mistamek.com.onetwoassignment.features.serp.presentation.filter.model

data class FilterVO(
    val id: Long?,
    val name: String
)