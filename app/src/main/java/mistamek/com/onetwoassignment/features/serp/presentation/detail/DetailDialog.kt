package mistamek.com.onetwoassignment.features.serp.presentation.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.default_fragment.*
import mistamek.com.onetwoassignment.R
import mistamek.com.onetwoassignment.core.App
import mistamek.com.onetwoassignment.features.serp.presentation.detail.di.DaggerDetailDialogComponent
import mistamek.com.onetwoassignment.features.serp.presentation.detail.di.DetailDialogModule
import mistamek.com.onetwoassignment.features.serp.presentation.detail.model.DetailVO
import mistamek.com.onetwoassignment.features.serp.presentation.detail.ui.DetailAdapter
import mistamek.com.onetwoassignment.util.gone
import mistamek.com.onetwoassignment.util.show
import mistamek.com.onetwoassignment.util.toast
import javax.inject.Inject

class DetailDialog : BottomSheetDialogFragment(), DetailView {

    companion object {
        private const val TOUR_ID = "DetailDialog.TourId"

        fun newInstance(tourId: Long) =
            DetailDialog().apply {
                arguments = Bundle().apply {
                    putLong(TOUR_ID, tourId)
                }
            }
    }

    @Inject
    lateinit var presenter: DetailPresenter

    private val detailAdapter = DetailAdapter { tourId, flightId ->
        requireContext().toast("Selected tourId $tourId and flightId $flightId")
        dismiss()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerDetailDialogComponent.builder()
            .applicationComponent((requireContext().applicationContext as App).applicationComponent)
            .detailDialogModule(DetailDialogModule(this))
            .build()
            .inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.default_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecycler()
        presenter.onViewReady(savedInstanceState == null)
    }

    override fun onStop() {
        super.onStop()
        presenter.onViewDestroy()
    }

    override fun showLoading() {
        loadingProgress.show()
    }

    override fun hideLoading() {
        loadingProgress.gone()
    }

    override fun showError(text: String) {
        context?.toast(text)
    }

    override fun showDetailVariants(items: List<DetailVO>) {
        detailAdapter.items = items
    }

    fun getSelectedTourId(): Long = arguments?.getLong(TOUR_ID) ?: throw IllegalStateException("not tour id was passed")

    private fun initRecycler() {
        with(contentRecyclerView) {
            adapter = detailAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }
}