package mistamek.com.onetwoassignment.features.serp.presentation.detail

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import mistamek.com.onetwoassignment.R
import mistamek.com.onetwoassignment.features.serp.domain.SerpInteractor
import mistamek.com.onetwoassignment.features.serp.presentation.detail.model.DetailVOMapper
import mistamek.com.onetwoassignment.util.plusAssign
import javax.inject.Inject

class DetailPresenterImpl @Inject constructor(
    private val tourId: Long,
    private val detailView: DetailView,
    private val serpInteractor: SerpInteractor,
    private val detailVOMapper: DetailVOMapper
) : DetailPresenter {

    private val compositeDisposable = CompositeDisposable()

    override fun onViewReady(wasRecreated: Boolean) {
        compositeDisposable += serpInteractor.getTours(true)
            .observeOn(Schedulers.computation())
            .map { tours -> tours.firstOrNull { tour -> tour.id == tourId } }
            .map { tour ->
                tour.flights.asSequence()
                    .sortedBy { it.price }
                    .map { detailVOMapper.mapToDetailVO(tour, it) }
                    .toList()
            }
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { detailView.showLoading() }
            .doFinally { detailView.hideLoading() }
            .subscribe({
                detailView.showDetailVariants(it)
            }, {
                detailView.showError(detailView.requireContext().getString(R.string.default_error))
            })
    }

    override fun onViewDestroy() {
        compositeDisposable.dispose()
    }
}