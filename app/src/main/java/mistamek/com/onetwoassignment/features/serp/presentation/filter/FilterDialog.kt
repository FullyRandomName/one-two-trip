package mistamek.com.onetwoassignment.features.serp.presentation.filter

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.default_fragment.*
import mistamek.com.onetwoassignment.R
import mistamek.com.onetwoassignment.core.App
import mistamek.com.onetwoassignment.features.serp.presentation.filter.di.DaggerFilterDialogComponent
import mistamek.com.onetwoassignment.features.serp.presentation.filter.di.FilterDialogModule
import mistamek.com.onetwoassignment.features.serp.presentation.filter.model.FilterVO
import mistamek.com.onetwoassignment.features.serp.presentation.filter.ui.FilterAdapter
import mistamek.com.onetwoassignment.util.gone
import mistamek.com.onetwoassignment.util.show
import mistamek.com.onetwoassignment.util.toast
import javax.inject.Inject

class FilterDialog : BottomSheetDialogFragment(), FilterView {

    companion object {
        const val SELECTED_COMPANY = "FilterDialog.SelectedCompany"
    }

    @Inject
    lateinit var presenter: FilterPresenter

    private val filterAdapter = FilterAdapter { companyId ->
        targetFragment?.onActivityResult(targetRequestCode, Activity.RESULT_OK, Intent().apply {
            if (companyId != null) {
                putExtra(SELECTED_COMPANY, companyId)
            }
        })
        dismiss()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerFilterDialogComponent.builder()
            .applicationComponent((requireContext().applicationContext as App).applicationComponent)
            .filterDialogModule(FilterDialogModule(this))
            .build()
            .inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.default_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecycler()
        presenter.onViewReady(savedInstanceState == null)
    }

    override fun onStop() {
        super.onStop()
        presenter.onViewDestroy()
    }

    override fun showLoading() {
        loadingProgress.show()
    }

    override fun hideLoading() {
        loadingProgress.gone()
    }

    override fun showError(text: String) {
        context?.toast(text)
    }

    override fun showCompanies(items: List<FilterVO>) {
        filterAdapter.items = items
    }

    private fun initRecycler() {
        with(contentRecyclerView) {
            adapter = filterAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }
}