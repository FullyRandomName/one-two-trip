package mistamek.com.onetwoassignment.features.serp.data.db.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "flight")
data class FlightEntity(
    @PrimaryKey var flightId: Long,
    @Embedded var company: CompanyEmbed,
    var departure: String,
    var arrival: String,
    var price: Long
) {
    data class CompanyEmbed(
        var companyId: Long,
        var name: String
    )
}