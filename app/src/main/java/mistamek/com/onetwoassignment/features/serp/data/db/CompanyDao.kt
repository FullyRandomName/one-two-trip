package mistamek.com.onetwoassignment.features.serp.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import mistamek.com.onetwoassignment.features.serp.data.db.model.CompanyEntity

@Dao
interface CompanyDao {

    @Query("SELECT * from company")
    fun getCompanies(): List<CompanyEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCompanies(flight: List<CompanyEntity>)

    @Query("DELETE from company")
    fun deleteCompanies()
}