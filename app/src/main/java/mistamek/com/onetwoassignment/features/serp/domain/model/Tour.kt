package mistamek.com.onetwoassignment.features.serp.domain.model

data class Tour(
    val id: Long,
    val flights: List<Flight>,
    val name: String,
    val price: Long
)