package mistamek.com.onetwoassignment.features.serp.presentation.filter.ui

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import mistamek.com.onetwoassignment.features.serp.presentation.filter.model.FilterVO

class FilterAdapter(private val clickListener: (item: Long?) -> Unit) : RecyclerView.Adapter<FilterViewHolder>() {

    var items: List<FilterVO> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilterViewHolder =
        FilterViewHolder(parent) { clickListener(items[it].id) }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: FilterViewHolder, position: Int) {
        holder.bind(items[position])
    }
}