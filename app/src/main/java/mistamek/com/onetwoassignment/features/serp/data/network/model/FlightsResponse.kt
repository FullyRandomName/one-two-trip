package mistamek.com.onetwoassignment.features.serp.data.network.model

data class FlightsResponse(
    val flights: List<FlightItem>
) {
    data class FlightItem(
        val id: Long,
        val companyId: Long,
        val departure: String,
        val arrival: String,
        val price: Long
    )
}