package mistamek.com.onetwoassignment.features.serp.domain.mapper

import androidx.collection.LongSparseArray
import mistamek.com.onetwoassignment.features.serp.data.db.model.CompanyEntity
import mistamek.com.onetwoassignment.features.serp.data.db.model.FlightEntity
import mistamek.com.onetwoassignment.features.serp.data.db.model.TourEntity
import mistamek.com.onetwoassignment.features.serp.domain.model.Company
import mistamek.com.onetwoassignment.features.serp.domain.model.Flight
import mistamek.com.onetwoassignment.features.serp.domain.model.Tour

object SerpEntityMapper {

    fun mapTourToEntity(tour: Tour, position: Int): TourEntity =
        tour.run {
            TourEntity(
                tourId = id,
                positionId = position,
                flightsIds = flights.map { it.id },
                name = name,
                price = price
            )
        }

    fun mapFlightToEntity(flight: Flight): FlightEntity =
        flight.run {
            FlightEntity(
                flightId = id,
                company = FlightEntity.CompanyEmbed(company.id, company.name),
                departure = departure,
                arrival = arrival,
                price = price
            )
        }

    fun mapCompanyToEntity(company: Company): CompanyEntity =
        company.run {
            CompanyEntity(
                companyId = id,
                name = name
            )
        }

    fun mapEntitiyToFlight(flight: FlightEntity): Flight =
        flight.run {
            Flight(
                id = flightId,
                departure = departure,
                arrival = arrival,
                price = price,
                company = Company(
                    id = company.companyId,
                    name = company.name
                )
            )
        }

    fun mapEntityToTour(tour: TourEntity, flightsArray: LongSparseArray<Flight>): Tour =
        tour.run {
            Tour(
                id = tourId,
                name = name,
                price = price,
                flights = flightsIds.mapNotNull { flightsArray[it] }
            )
        }

    fun mapEntityToCompany(companyEntity: CompanyEntity): Company =
        companyEntity.run {
            Company(
                id = companyId,
                name = name
            )
        }
}