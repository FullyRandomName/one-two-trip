package mistamek.com.onetwoassignment.features.serp.data

import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import mistamek.com.onetwoassignment.features.serp.data.db.SerpDatabase
import mistamek.com.onetwoassignment.features.serp.data.db.model.CompanyEntity
import mistamek.com.onetwoassignment.features.serp.data.db.model.FlightEntity
import mistamek.com.onetwoassignment.features.serp.data.db.model.TourEntity
import mistamek.com.onetwoassignment.features.serp.data.network.SerpApi
import mistamek.com.onetwoassignment.features.serp.data.network.model.CompaniesResponse
import mistamek.com.onetwoassignment.features.serp.data.network.model.FlightsResponse
import mistamek.com.onetwoassignment.features.serp.data.network.model.HotelsResponse
import javax.inject.Inject

interface SerpRepository {

    fun getFlights(): Single<FlightsResponse>

    fun getHotels(): Single<HotelsResponse>

    fun getCompanies(): Single<CompaniesResponse>

    fun saveTours(tours: List<TourEntity>)

    fun saveFlights(flights: List<FlightEntity>)

    fun saveCompanies(companies: List<CompanyEntity>)

    fun getToursCached(): Single<List<TourEntity>>

    fun getFlightsCached(): Single<List<FlightEntity>>

    fun getCompaniesCached(): Single<List<CompanyEntity>>
}

class SerpRepositoryImpl @Inject constructor(
    private val serpApi: SerpApi,
    private val serpDatabase: SerpDatabase
) : SerpRepository {

    override fun getFlights(): Single<FlightsResponse> =
        serpApi.getFlights()
            .subscribeOn(Schedulers.io())

    override fun getHotels(): Single<HotelsResponse> =
        serpApi.getHotels()
            .subscribeOn(Schedulers.io())

    override fun getCompanies(): Single<CompaniesResponse> =
        serpApi.getCompanies()
            .subscribeOn(Schedulers.io())

    override fun saveTours(tours: List<TourEntity>) {
        serpDatabase.tourDao().apply {
            deleteTours()
            insertTours(tours)
        }
    }

    override fun saveFlights(flights: List<FlightEntity>) {
        serpDatabase.flightsDao().apply {
            deleteFlights()
            insertFlights(flights)
        }
    }

    override fun saveCompanies(companies: List<CompanyEntity>) {
        serpDatabase.companiesDao().apply {
            deleteCompanies()
            insertCompanies(companies)
        }
    }

    override fun getToursCached(): Single<List<TourEntity>> =
        Single.fromCallable {
            serpDatabase.tourDao().getTours()
        }.subscribeOn(Schedulers.io())

    override fun getFlightsCached(): Single<List<FlightEntity>> =
        Single.fromCallable {
            serpDatabase.flightsDao().getFlights()
        }.subscribeOn(Schedulers.io())

    override fun getCompaniesCached(): Single<List<CompanyEntity>> =
        Single.fromCallable {
            serpDatabase.companiesDao().getCompanies()
        }.subscribeOn(Schedulers.io())
}