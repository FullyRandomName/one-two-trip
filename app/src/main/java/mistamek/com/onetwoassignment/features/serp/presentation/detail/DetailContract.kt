package mistamek.com.onetwoassignment.features.serp.presentation.detail

import mistamek.com.onetwoassignment.core.platform.BasePresenter
import mistamek.com.onetwoassignment.core.platform.BaseView
import mistamek.com.onetwoassignment.features.serp.presentation.detail.model.DetailVO

interface DetailView : BaseView {

    fun showLoading()

    fun hideLoading()

    fun showError(text: String)

    fun showDetailVariants(items: List<DetailVO>)
}

interface DetailPresenter : BasePresenter {
}