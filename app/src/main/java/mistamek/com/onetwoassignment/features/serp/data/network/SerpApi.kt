package mistamek.com.onetwoassignment.features.serp.data.network

import io.reactivex.Single
import mistamek.com.onetwoassignment.features.serp.data.network.model.CompaniesResponse
import mistamek.com.onetwoassignment.features.serp.data.network.model.FlightsResponse
import mistamek.com.onetwoassignment.features.serp.data.network.model.HotelsResponse
import retrofit2.http.GET

interface SerpApi {

    @GET("zqxvw")
    fun getFlights(): Single<FlightsResponse>

    @GET("12q3ws")
    fun getHotels(): Single<HotelsResponse>

    @GET("8d024")
    fun getCompanies(): Single<CompaniesResponse>
}