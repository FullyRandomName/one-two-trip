package mistamek.com.onetwoassignment.features.serp.domain

import androidx.collection.LongSparseArray
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function3
import io.reactivex.schedulers.Schedulers
import mistamek.com.onetwoassignment.features.serp.data.SerpRepository
import mistamek.com.onetwoassignment.features.serp.data.db.model.FlightEntity
import mistamek.com.onetwoassignment.features.serp.data.db.model.TourEntity
import mistamek.com.onetwoassignment.features.serp.data.network.model.CompaniesResponse
import mistamek.com.onetwoassignment.features.serp.data.network.model.FlightsResponse
import mistamek.com.onetwoassignment.features.serp.data.network.model.HotelsResponse
import mistamek.com.onetwoassignment.features.serp.domain.mapper.SerpEntityMapper
import mistamek.com.onetwoassignment.features.serp.domain.mapper.SerpResponseMapper
import mistamek.com.onetwoassignment.features.serp.domain.model.Company
import mistamek.com.onetwoassignment.features.serp.domain.model.Flight
import mistamek.com.onetwoassignment.features.serp.domain.model.Tour
import javax.inject.Inject

interface SerpInteractor {
    fun getTours(canUserCache: Boolean): Single<List<Tour>>

    fun getCompanies(): Single<List<Company>>
}

class SerpInteractorImpl @Inject constructor(
    private val serpRepository: SerpRepository
) : SerpInteractor {

    override fun getTours(canUserCache: Boolean): Single<List<Tour>> =
        if (canUserCache) {
            getToursFromCache()
        } else {
            getToursFromNetwork()
        }

    private fun getToursFromCache(): Single<List<Tour>> =
        Single.zip<List<FlightEntity>, List<TourEntity>, List<Tour>>(
            serpRepository.getFlightsCached(),
            serpRepository.getToursCached(),
            BiFunction { flights, tours ->
                if (flights.isEmpty() || tours.isEmpty()) {
                    throw IllegalStateException("Trying to get empty cache")
                }

                val flightsArray = LongSparseArray<Flight>()
                flights.map { SerpEntityMapper.mapEntitiyToFlight(it) }.forEach {
                    flightsArray.put(it.id, it)
                }
                tours.map { SerpEntityMapper.mapEntityToTour(it, flightsArray) }
            }
        ).onErrorResumeNext { getToursFromNetwork() }

    private fun getToursFromNetwork(): Single<List<Tour>> =
        Single.zip<HotelsResponse, FlightsResponse, CompaniesResponse, Triple<HotelsResponse, FlightsResponse, CompaniesResponse>>(
            serpRepository.getHotels(),
            serpRepository.getFlights(),
            serpRepository.getCompanies(),
            Function3 { hotels, flights, companies ->
                Triple(hotels, flights, companies)
            })
            .observeOn(Schedulers.computation())
            .map { triple ->
                val companiesSparseArray = LongSparseArray<Company>()
                val companies = triple.third.companies.map { SerpResponseMapper.mapCompanyResponseToCompany(it) }
                companies.forEach {
                    companiesSparseArray.put(it.id, it)
                }

                val flightsSparseArray = LongSparseArray<Flight>()
                val flights =
                    triple.second.flights.map { SerpResponseMapper.mapFlightResponseToFlight(it, companiesSparseArray) }
                flights.forEach {
                    flightsSparseArray.put(it.id, it)
                }

                val tours =
                    triple.first.hotels.map { SerpResponseMapper.mapHotelResponseToTour(it, flightsSparseArray) }

                Triple(tours, flights, companies)
            }
            .flatMap { argPair ->
                Single.just(argPair)
                    .observeOn(Schedulers.io())
                    .doOnSuccess { pair ->
                        serpRepository.saveTours(pair.first.mapIndexed { index, tour ->
                            SerpEntityMapper.mapTourToEntity(tour, index)
                        })
                        serpRepository.saveFlights(pair.second.map { flight ->
                            SerpEntityMapper.mapFlightToEntity(flight)
                        })
                        serpRepository.saveCompanies(pair.third.map { company ->
                            SerpEntityMapper.mapCompanyToEntity(company)
                        })
                    }
                    .map { it.first }
                    .onErrorReturn { argPair.first }
            }

    override fun getCompanies(): Single<List<Company>> =
        serpRepository.getCompaniesCached()
            .observeOn(Schedulers.computation())
            .map { companies ->
                if (companies.isEmpty()) {
                    throw IllegalStateException("Trying to get empty cash")
                }
                companies.map { SerpEntityMapper.mapEntityToCompany(it) }
            }.onErrorResumeNext {
                serpRepository.getCompanies()
                    .observeOn(Schedulers.computation())
                    .map { companiesResponse ->
                        companiesResponse.companies.map { company ->
                            SerpResponseMapper.mapCompanyResponseToCompany(company)
                        }
                    }
            }
}