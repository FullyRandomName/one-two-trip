package mistamek.com.onetwoassignment.features.serp.presentation.filter

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import mistamek.com.onetwoassignment.R
import mistamek.com.onetwoassignment.features.serp.domain.SerpInteractor
import mistamek.com.onetwoassignment.features.serp.presentation.filter.model.FilterVO
import mistamek.com.onetwoassignment.features.serp.presentation.filter.model.FilterVOMapper
import mistamek.com.onetwoassignment.util.plusAssign
import javax.inject.Inject

class FilterPresenterImpl @Inject constructor(
    private val filterView: FilterView,
    private val serpInteractor: SerpInteractor,
    private val filterVOMapper: FilterVOMapper
) : FilterPresenter {

    private val compositeDisposable = CompositeDisposable()

    override fun onViewReady(wasRecreated: Boolean) {
        compositeDisposable += serpInteractor.getCompanies()
            .map { companies ->
                companies.asSequence()
                    .map { filterVOMapper.mapCompanyToCompanyVO(it) }
                    .plus(FilterVO(null, filterView.requireContext().getString(R.string.serp_filter_cancel)))
                    .toList()
            }
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { filterView.showLoading() }
            .doFinally { filterView.hideLoading() }
            .subscribe({
                filterView.showCompanies(it)
            }, {
                filterView.showError(filterView.requireContext().getString(R.string.default_error))
            })
    }

    override fun onViewDestroy() {
        compositeDisposable.dispose()
    }
}