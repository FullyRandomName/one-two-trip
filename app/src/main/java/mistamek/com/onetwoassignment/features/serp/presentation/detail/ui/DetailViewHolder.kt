package mistamek.com.onetwoassignment.features.serp.presentation.detail.ui

import android.view.ViewGroup
import kotlinx.android.synthetic.main.serp_detail_view_holder.*
import mistamek.com.onetwoassignment.R
import mistamek.com.onetwoassignment.core.platform.BaseViewHolder
import mistamek.com.onetwoassignment.features.serp.presentation.detail.model.DetailVO

class DetailViewHolder(parent: ViewGroup, clickListener: (position: Int) -> Unit) :
    BaseViewHolder(R.layout.serp_detail_view_holder, parent) {

    init {
        itemView.setOnClickListener { clickListener(adapterPosition) }
    }

    fun bind(item: DetailVO) {
        variantNameTextView.text = item.name
        variantPriceTextView.text = item.price
    }
}