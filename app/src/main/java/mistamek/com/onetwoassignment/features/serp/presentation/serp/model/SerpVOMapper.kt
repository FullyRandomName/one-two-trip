package mistamek.com.onetwoassignment.features.serp.presentation.serp.model

import android.content.res.Resources
import mistamek.com.onetwoassignment.R
import mistamek.com.onetwoassignment.features.serp.domain.model.Tour
import javax.inject.Inject

interface SerpVOMapper {
    fun mapTourToTourVO(tour: Tour): TourVO
}

class SerpVOMapperImpl @Inject constructor(
    private val resources: Resources
) : SerpVOMapper {

    override fun mapTourToTourVO(tour: Tour): TourVO {
        val flightsSize = tour.flights.size

        val cheapestFlightPrice = tour.flights.sortedBy { it.price }.firstOrNull()?.price ?: 0
        val price = cheapestFlightPrice + tour.price
        val priceString = StringBuilder()
            .apply {
                if (flightsSize > 1) {
                    append(resources.getString(R.string.serp_from))
                    append(" ")
                }
            }
            .append(resources.getString(R.string.serp_price_description, price))

        return TourVO(
            id = tour.id,
            name = tour.name,
            priceDescription = priceString.toString(),
            variantsDescription = resources.getQuantityString(
                R.plurals.serp_variants_description,
                flightsSize,
                flightsSize
            )
        )
    }
}