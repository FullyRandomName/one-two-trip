package mistamek.com.onetwoassignment.features.serp.domain.model

data class Company(
    val id: Long,
    val name: String
)