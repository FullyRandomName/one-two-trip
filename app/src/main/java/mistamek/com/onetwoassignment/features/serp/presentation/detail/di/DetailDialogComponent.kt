package mistamek.com.onetwoassignment.features.serp.presentation.detail.di

import dagger.Component
import mistamek.com.onetwoassignment.di.ApplicationComponent
import mistamek.com.onetwoassignment.di.FragmentScope
import mistamek.com.onetwoassignment.features.serp.presentation.detail.DetailDialog

@FragmentScope
@Component(dependencies = [ApplicationComponent::class], modules = [DetailDialogModule::class])
interface DetailDialogComponent {

    fun inject(dialog: DetailDialog)
}