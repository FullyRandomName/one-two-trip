package mistamek.com.onetwoassignment.features.serp.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import mistamek.com.onetwoassignment.features.serp.data.db.model.CompanyEntity
import mistamek.com.onetwoassignment.features.serp.data.db.model.FlightEntity
import mistamek.com.onetwoassignment.features.serp.data.db.model.TourEntity
import mistamek.com.onetwoassignment.util.typeconverters.LongListTypeConverter

@Database(
    entities = [TourEntity::class, FlightEntity::class, CompanyEntity::class],
    version = 1
)
@TypeConverters(LongListTypeConverter::class)
abstract class SerpDatabase : RoomDatabase() {

    companion object {
        const val DB_NAME = "serp"
    }

    abstract fun tourDao(): TourDao

    abstract fun flightsDao(): FlightDao

    abstract fun companiesDao(): CompanyDao
}