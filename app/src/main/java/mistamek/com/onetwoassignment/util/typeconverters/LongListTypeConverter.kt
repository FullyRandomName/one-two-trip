package mistamek.com.onetwoassignment.util.typeconverters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class LongListTypeConverter {

    private val gson = Gson()

    @TypeConverter
    fun StringToListLong(string: String?): List<Long>? =
        string?.let {
            val type = object : TypeToken<List<Long>>() {
            }.type
            gson.fromJson<List<Long>>(it, type)
        } ?: emptyList()

    @TypeConverter
    fun listLongToString(list: List<Long>?): String? {
        return gson.toJson(list)
    }
}