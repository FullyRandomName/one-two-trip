package mistamek.com.onetwoassignment.core

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import mistamek.com.onetwoassignment.R
import mistamek.com.onetwoassignment.features.serp.presentation.serp.SerpFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (supportFragmentManager.findFragmentById(R.id.mainFrame) == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.mainFrame, SerpFragment())
                .commit()
        }
    }
}
