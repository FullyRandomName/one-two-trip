package mistamek.com.onetwoassignment.core

import android.app.Application
import mistamek.com.onetwoassignment.di.ApplicationComponent
import mistamek.com.onetwoassignment.di.ApplicationModule
import mistamek.com.onetwoassignment.di.DaggerApplicationComponent

class App : Application() {

    lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        initDagger()
    }

    private fun initDagger() {
        applicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }
}