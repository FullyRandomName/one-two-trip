package mistamek.com.onetwoassignment.core.platform

import android.content.Context

interface BaseView {

    fun requireContext(): Context
}

interface BasePresenter {

    fun onViewReady(wasRecreated: Boolean)

    fun onViewDestroy()
}