package mistamek.com.onetwoassignment.di

import android.content.Context
import android.content.res.Resources
import dagger.Module
import dagger.Provides
import mistamek.com.onetwoassignment.core.App
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: App) {

    @Provides
    @Singleton
    fun provideApplicationContext(): Context = application

    @Provides
    @Singleton
    fun provideResources(): Resources = application.resources
}