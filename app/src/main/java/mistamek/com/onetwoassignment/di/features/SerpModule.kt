package mistamek.com.onetwoassignment.di.features

import android.content.Context
import androidx.room.Room
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.Reusable
import mistamek.com.onetwoassignment.features.serp.data.SerpRepository
import mistamek.com.onetwoassignment.features.serp.data.SerpRepositoryImpl
import mistamek.com.onetwoassignment.features.serp.data.db.SerpDatabase
import mistamek.com.onetwoassignment.features.serp.data.network.SerpApi
import mistamek.com.onetwoassignment.features.serp.domain.SerpInteractor
import mistamek.com.onetwoassignment.features.serp.domain.SerpInteractorImpl
import retrofit2.Retrofit

@Module
abstract class SerpModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun providesSerpApi(retrofit: Retrofit): SerpApi = retrofit.create(
            SerpApi::class.java
        )

        @Provides
        @JvmStatic
        fun provideSerpDatabase(context: Context): SerpDatabase =
            Room.databaseBuilder(
                context.applicationContext,
                SerpDatabase::class.java,
                SerpDatabase.DB_NAME
            ).build()
    }

    @Binds
    abstract fun bindSerpRepository(impl: SerpRepositoryImpl): SerpRepository

    @Binds
    @Reusable
    abstract fun bindSerpInteractor(impl: SerpInteractorImpl): SerpInteractor
}