package mistamek.com.onetwoassignment.di

import javax.inject.Scope

@Retention(value = AnnotationRetention.RUNTIME)
@Scope
annotation class FragmentScope