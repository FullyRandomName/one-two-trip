package mistamek.com.onetwoassignment.di

import android.content.Context
import android.content.res.Resources
import dagger.Component
import mistamek.com.onetwoassignment.di.features.SerpModule
import mistamek.com.onetwoassignment.features.serp.domain.SerpInteractor
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ApplicationModule::class,
        NetworkModule::class,
        SerpModule::class
    ]
)
interface ApplicationComponent {

    fun getContext(): Context

    fun getResources(): Resources

    fun getSerInteractor(): SerpInteractor
}